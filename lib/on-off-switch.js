var OnOffSwitch = (function () {
    function OnOffSwitch(element, onActive, onInactive) {
        this._isActive = false;
        this.element = element;
        this.parts = {
            handle: element.querySelector(".handle"),
            checkbox: element.querySelector("input[type=checkbox]")
        };
        this.onActive = onActive;
        this.onInactive = onInactive;
        this.element.addEventListener("click", this._onClick.bind(this));
        var isActive = !!this.element.dataset["checked"];
        if (this.parts.checkbox)
            isActive = this.parts.checkbox.checked;
        this.toggle(isActive);
    }
    OnOffSwitch.prototype._onClick = function () {
        this.toggle();
    };
    OnOffSwitch.prototype.isActive = function () {
        return this._isActive;
    };
    OnOffSwitch.prototype.toggle = function (force) {
        if (force === void 0) { force = !this._isActive; }
        if (this.parts.checkbox)
            this.parts.checkbox.checked = force;
        this._isActive = force;
        this.element.classList.toggle("is-active", force);
        if (force && this.onActive)
            this.onActive();
        if (!force && this.onInactive)
            this.onInactive();
        return force;
    };
    return OnOffSwitch;
}());
document.addEventListener("DOMContentLoaded", function () {
    var elements = document.querySelectorAll(".on-off-switch");
    for (var i = 0; i < elements.length; i++) {
        new OnOffSwitch(elements.item(i));
    }
});
