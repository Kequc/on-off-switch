class OnOffSwitch
{
    private _isActive: boolean = false;

    onActive: Function;
    onInactive: Function;

    element: HTMLElement;
    parts: {
        handle: HTMLElement,
        checkbox: HTMLInputElement
    };

    constructor (element: HTMLElement, onActive?: Function, onInactive?: Function)
    {
        this.element = element;
        this.parts = {
            handle: <HTMLElement>element.querySelector(".handle"),
            checkbox: <HTMLInputElement>element.querySelector("input[type=checkbox]")
        };

        this.onActive = onActive;
        this.onInactive = onInactive;
        this.element.addEventListener("click", this._onClick.bind(this));

        let isActive = !!this.element.dataset["checked"];
        if (this.parts.checkbox)
            isActive = this.parts.checkbox.checked;
        
        this.toggle(isActive);
    }

    private _onClick ()
    {
        this.toggle();
    }

    isActive (): boolean
    {
        return this._isActive;
    }

    toggle (force: boolean = !this._isActive): boolean
    {
        if (this.parts.checkbox)
            this.parts.checkbox.checked = force;
        this._isActive = force;

        this.element.classList.toggle("is-active", force);

        if (force && this.onActive)
            this.onActive();
        if (!force && this.onInactive)
            this.onInactive();

        return force;
    }
}

document.addEventListener("DOMContentLoaded", () => {
    let elements = document.querySelectorAll(".on-off-switch");
    for (let i = 0; i < elements.length; i++) {
        new OnOffSwitch(<HTMLElement>elements.item(i));
    }
});
